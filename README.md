# sokoban

A single-player puzzle game.

Move the blue dot with the arrow keys to push the red blocks into the orange zones. Don't get stuck!

Built with HTML, CSS, and JavaScript.
