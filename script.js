//Game Board Layouts
const map1 = [
  "  WWWWW ",
  "WWW   W ",
  "WOSB  W ",
  "WWW BOW ",
  "WOWWB W ",
  "W W O WW",
  "WB XBBOW",
  "W   O  W",
  "WWWWWWWW",
];

const map2 = [
  "    WWWWW          ",
  "    W   W          ",
  "    WB  W          ",
  "  WWW  BWW         ",
  "  W  B B W         ",
  "WWW W WW W   WWWWWW",
  "W   W WW WWWWW  OOW",
  "W B  B          OOW",
  "WWWWW WWW WSWW  OOW",
  "    W     WWWWWWWWW",
  "    WWWWWWW        ",
];

const map3 = [
  "    WWWWW              WWWWW          ",
  "    W   W              W   W          ",
  "    WB  W              WB  W          ",
  "  WWW  BWW           WWW  BWW         ",
  "  W  B B W           W  B B W         ",
  "WWW W WW W   WWWWWWWWW W WW W   WWWWWW",
  "W   W WW WWWWW  OO     W WW WWWWW  OOW",
  "W B  B          OOWW B  B          OOW",
  "WWWWW WWW WSWW  OOWWWWWW WWW W WW  OOW",
  "    W     WWWWWWWWW    W     WWWWWWWWW",
  "    WWWWWWW            WWWWWWW        ",
];

//Global variables
const mapCurrent = map3;
let scoreGoal = 0;
let scoreCurrent = 0;
let player1 = new Player(0, 0);
let playerCurrent = player1;
let boxes = [];
let boxCurrent;

//Event listeners
buttonReset.addEventListener("click", function () {
  location.reload();
});

//Static & live game board models
//Static model is used for referencing locations of storage cells.
//Dynamic model is updated to track locations of boxes.

function createMapStatic(textMap) {
  let rowIndex = 0;
  let rowCount = textMap.length;
  let resultArray = [];
  while (rowIndex < rowCount) {
    resultArray[rowIndex] = textMap[rowIndex].split("");
    resultArray[rowIndex].forEach(function (item, i) {
      if (item == "B") resultArray[rowIndex][i] = " ";
    });
    resultArray[rowIndex].forEach(function (item, i) {
      if (item == "X") {
        resultArray[rowIndex][i] = "O";
        scoreCurrent++;
      }
    });
    rowIndex++;
  }
  return resultArray;
}

let mapStatic = createMapStatic(mapCurrent);

function createMapDynamic(textMap) {
  let rowIndex = 0;
  let rowCount = textMap.length;
  let resultArray = [];
  while (rowIndex < rowCount) {
    resultArray[rowIndex] = textMap[rowIndex].split("");
    resultArray[rowIndex].forEach(function (item, i) {
      if (item == "B" || item == "X") {
        boxes.push(new Box(rowIndex, i));
        scoreGoal++;
      }
    });
    resultArray[rowIndex].forEach(function (item, i) {
      if (item == "S") {
        player1.row = rowIndex;
        player1.cell = i;
        setPlayerSource();
      }
    });
    rowIndex++;
  }
  return resultArray;
}

let mapDynamic = createMapDynamic(mapCurrent);

//Player object
function Player(playerStartingRow, playerStartingCell) {
  this.row = playerStartingRow;
  this.cell = playerStartingCell;
  this.sourceRow = playerStartingRow;
  this.sourceCell = playerStartingCell;
  this.destinationRow = playerStartingRow;
  this.destinationCell = playerStartingCell;
  addEventListener("keydown", tryPlayerMove);
}
function setPlayerSource() {
  playerCurrent.sourceRow = playerCurrent.row;
  playerCurrent.sourceCell = playerCurrent.cell;
}
function setPlayerDestination(rowChange, cellChange) {
  playerCurrent.destinationRow = playerCurrent.row + rowChange;
  playerCurrent.destinationCell = playerCurrent.cell + cellChange;
}
function pushBox(direction) {
  tryBoxMove(direction);
}

function chooseBox(targetRow, targetCell) {
  let boxFilter = boxes.filter((x) => x.row === targetRow);
  boxFilter = boxFilter.filter((x) => x.cell === targetCell);
  boxCurrent = boxFilter[0];
}

function movePlayer(rowChange, cellChange) {
  console.log(
    "player coordinates = [" +
      playerCurrent.row +
      "][" +
      playerCurrent.cell +
      "]"
  );
  setPlayerSourceDiv();
  setPlayerDestinationDiv();
  playerCurrent.row += rowChange;
  playerCurrent.cell += cellChange;
  console.log(
    "player new coordinates = [" +
      playerCurrent.row +
      "][" +
      playerCurrent.cell +
      "]"
  );
  setPlayerSource();
  drawPlayer();
}

let currentDirection;

function tryPlayerMove(e) {
  console.log(e.code);
  currentDirection = e.code;
  let rowChange;
  let cellChange;
  let targetRow;
  let targetCell;
  let targetCellType;

  function moveSwitch(cellType) {
    switch (cellType) {
      case "B": {
        chooseBox(targetRow, targetCell);
        pushBox(currentDirection);
        break;
      }
      case "X": {
        chooseBox(targetRow, targetCell);
        pushBox(currentDirection);
        break;
      }
      case "W": {
        bump();
        break;
      }
      default: {
        if (!checkVacant(cellType)) break;
        movePlayer(rowChange, cellChange);
        break;
      }
    }
  }

  switch (e.code) {
    case "ArrowDown": {
      rowChange = 1;
      cellChange = 0;
      setPlayerDestination(rowChange, cellChange);
      targetRow = playerCurrent.destinationRow;
      targetCell = playerCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetRow < 0 || targetRow >= mapStatic.length) return;
      moveSwitch(targetCellType);
      break;
    }
    case "ArrowUp": {
      rowChange = -1;
      cellChange = 0;
      setPlayerDestination(rowChange, cellChange);
      targetRow = playerCurrent.destinationRow;
      targetCell = playerCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetRow < 0 || targetRow >= mapStatic.length) return;
      moveSwitch(targetCellType);
      break;
    }
    case "ArrowLeft": {
      rowChange = 0;
      cellChange = -1;
      setPlayerDestination(rowChange, cellChange);
      targetRow = playerCurrent.destinationRow;
      targetCell = playerCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetCell < 0 || targetCell >= mapStatic[targetRow].length) return;
      moveSwitch(targetCellType);
      break;
    }
    case "ArrowRight": {
      rowChange = 0;
      cellChange = 1;
      setPlayerDestination(rowChange, cellChange);
      targetRow = playerCurrent.destinationRow;
      targetCell = playerCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetCell < 0 || targetCell >= mapStatic[targetRow].length) return;
      moveSwitch(targetCellType);
      break;
    }
  }
}
Player.prototype = {
  constructor: Player,
  setPlayerSource,
  setPlayerDestination,
  tryPlayerMove,
  pushBox,
  movePlayer,
  // makePlayerDiv
};

//Box objects
function Box(boxStartingRow, boxStartingCell) {
  this.boxId = scoreGoal;
  this.row = boxStartingRow;
  this.cell = boxStartingCell;
  this.sourceRow = boxStartingRow;
  this.sourceCell = boxStartingCell;
  this.destinationRow;
  this.destinationCell;
}
function setBoxSource() {
  boxCurrent.sourceRow = boxCurrent.row;
  boxCurrent.sourceCell = boxCurrent.cell;
}
function setBoxDestination(rowChange, cellChange) {
  boxCurrent.destinationRow = boxCurrent.row + rowChange;
  boxCurrent.destinationCell = boxCurrent.cell + cellChange;
}

function tryBoxMove(direction) {
  let currentDirection = direction;
  let rowChange;
  let cellChange;
  let targetRow;
  let targetCell;
  let targetCellType;

  function moveSwitch(cellType) {
    switch (cellType) {
      case "B": {
        bump();
        break;
      }
      case "W": {
        bump();
        break;
      }
      case "X": {
        bump();
        break;
      }
      default: {
        if (!checkVacant(cellType)) break;
        moveBox(rowChange, cellChange);
        break;
      }
    }
  }

  switch (currentDirection) {
    case "ArrowDown": {
      rowChange = 1;
      cellChange = 0;
      setBoxDestination(rowChange, cellChange);
      targetRow = boxCurrent.destinationRow;
      targetCell = boxCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetRow < 0 || targetRow >= mapStatic.length) return;
      moveSwitch(targetCellType);
      break;
    }
    case "ArrowUp": {
      rowChange = -1;
      cellChange = 0;
      setBoxDestination(rowChange, cellChange);
      targetRow = boxCurrent.destinationRow;
      targetCell = boxCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetRow < 0 || targetRow >= mapStatic.length) return;
      moveSwitch(targetCellType);
      break;
    }
    case "ArrowLeft": {
      rowChange = 0;
      cellChange = -1;
      setBoxDestination(rowChange, cellChange);
      targetRow = boxCurrent.destinationRow;
      targetCell = boxCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetCell < 0 || targetCell >= mapStatic[targetRow].length) return;
      moveSwitch(targetCellType);
      break;
    }
    case "ArrowRight": {
      rowChange = 0;
      cellChange = 1;
      setBoxDestination(rowChange, cellChange);
      targetRow = boxCurrent.destinationRow;
      targetCell = boxCurrent.destinationCell;
      targetCellType = mapDynamic[targetRow][targetCell];
      if (targetCell < 0 || targetCell >= mapStatic[targetRow].length) return;
      moveSwitch(targetCellType);
      break;
    }
  }
}

function moveBox(rowChange, cellChange) {
  console.log(
    "box coordinates = [" + boxCurrent.row + "][" + boxCurrent.cell + "]"
  );
  //update box parameters
  setBoxSourceDiv();
  boxCurrent.row += rowChange;
  boxCurrent.cell += cellChange;
  setBoxDestinationDiv();
  console.log(
    "box new coordinates = [" + boxCurrent.row + "][" + boxCurrent.cell + "]"
  );
  //update dynamic map
  mapDynamic[boxCurrent.sourceRow][boxCurrent.sourceCell] =
    mapStatic[boxCurrent.sourceRow][boxCurrent.sourceCell];
  mapDynamic[boxCurrent.row][boxCurrent.cell] = "B";
  //check for win
  if (
    getCellType(boxCurrent.sourceRow, boxCurrent.sourceCell) === "O" ||
    getCellType(boxCurrent.sourceRow, boxCurrent.sourceCell) === "X"
  ) {
    scoreCurrent -= 1;
  }
  if (
    getCellType(boxCurrent.row, boxCurrent.cell) === "O" ||
    getCellType(boxCurrent.sourceRow, boxCurrent.sourceCell) === "X"
  ) {
    scoreCurrent += 1;
    checkWin();
  }
  setBoxSource();
  drawBox();
  //move player
  movePlayer(rowChange, cellChange);
}

Box.prototype = {
  constructor: Box,
  setBoxSource,
  setBoxDestination,
  tryBoxMove,
  moveBox,
};

function checkVacant(cellType) {
  switch (cellType) {
    case " ": {
      return true;
      break;
    }
    case "O": {
      return true;
      break;
    }
    case "S": {
      return true;
      break;
    }
    case "X": {
      return true;
      break;
    }
    case "W": {
      return false;
      break;
    }
  }
}

function getCellType(row, cell) {
  return mapStatic[row][cell];
}

function checkWin() {
  if (scoreCurrent === scoreGoal) win();
}

function win() {
  windicator.innerHTML = "You Won!";
  buttonReset.className = "visible";
}

function bump() {
  console.log("bump!");
}

//DOM Stuff

const boardDiv = document.querySelector("#gameBoard");

const player1div = document.createElement("div");
player1div.className = "player";

let playerSourceDiv;
let playerDestinationDiv;
let boxSourceDiv;
let boxDestinationDiv;

function drawMazeArray(paramMapDynamic) {
  let rowIndex = 0;
  let rowCount = paramMapDynamic.length;
  let cellIndex = 0;
  let cellCount = paramMapDynamic[0].length;
  let boxDivCount = 0;

  function makeBoxDiv() {
    let newBoxDiv = document.createElement("div");
    newBoxDiv.className = "box";
    newBoxDiv.dataset.boxDivId = boxDivCount;
    boxDivCount++;
    return newBoxDiv;
  }

  while (rowIndex < rowCount) {
    let newRowDiv = document.createElement("div");
    newRowDiv.className = "row";
    cellIndex = 0;
    cellCount = paramMapDynamic[rowIndex].length;
    while (cellIndex < cellCount) {
      let newCellDiv = document.createElement("div");
      let newBoxDiv;
      newCellDiv.className = "cell";
      newCellDiv.dataset.cellIndex = cellIndex;
      newCellDiv.dataset.rowIndex = rowIndex;
      switch (paramMapDynamic[rowIndex][cellIndex]) {
        case "W":
          newCellDiv.className += " cellWall";
          break;
        case "S":
          newCellDiv.className += " cellStart";
          newCellDiv.appendChild(player1div);
          break;
        case "O":
          newCellDiv.className += " cellStorage";
          break;
        case "B":
          newCellDiv.className += " cellFloor";
          newBoxDiv = makeBoxDiv(rowIndex, cellIndex);
          newCellDiv.appendChild(newBoxDiv);
          break;
        case "X":
          newCellDiv.className += " cellStorage";
          newBoxDiv = makeBoxDiv(rowIndex, cellIndex);
          newCellDiv.appendChild(newBoxDiv);
          break;
        default:
          newCellDiv.className += " cellFloor";
      }
      newRowDiv.appendChild(newCellDiv);
      cellIndex++;
    }
    boardDiv.appendChild(newRowDiv);
    rowIndex++;
  }
}

drawMazeArray(mapDynamic);

function setPlayerSourceDiv() {
  playerSourceDiv = document.querySelector(
    "[data-row-index=" +
      CSS.escape(playerCurrent.row) +
      "][data-cell-index=" +
      CSS.escape(playerCurrent.cell) +
      "]"
  );
}

function setPlayerDestinationDiv() {
  playerDestinationDiv = document.querySelector(
    "[data-row-index=" +
      CSS.escape(playerCurrent.destinationRow) +
      "][data-cell-index=" +
      CSS.escape(playerCurrent.destinationCell) +
      "]"
  );
}

function setBoxSourceDiv() {
  console.log("setting Box Source Div");
  boxSourceDiv = document.querySelector(
    "[data-row-index=" +
      CSS.escape(boxCurrent.row) +
      "][data-cell-index=" +
      CSS.escape(boxCurrent.cell) +
      "]"
  );
}

function setBoxDestinationDiv() {
  console.log("setting Box Destination Div");
  boxDestinationDiv = document.querySelector(
    "[data-row-index=" +
      CSS.escape(boxCurrent.row) +
      "][data-cell-index=" +
      CSS.escape(boxCurrent.cell) +
      "]"
  );
}

function drawPlayer() {
  playerDestinationDiv.appendChild(playerSourceDiv.lastElementChild);
}

function drawBox() {
  boxDestinationDiv.appendChild(boxSourceDiv.lastElementChild);
}
